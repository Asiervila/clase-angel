package com.atos.modelo;

import java.util.List;
import java.io.Serializable;
import javax.persistence.*;


/*
 * Entidad JPA para tabla HR.REGIONS
 * 
 */
@Entity
@Table(name="REGIONS", schema="HR")
public class Region implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="REGION_ID")
	private int regionId;

	@Column(name="REGION_NAME")
	private String regionName;
	
	@OneToOne(fetch= FetchType.LAZY, mappedBy="region", cascade= CascadeType.ALL)
	private Desc_reg descripcion;
	
	@OneToMany(fetch= FetchType.EAGER, mappedBy="region", cascade= CascadeType.ALL)  
	private List<Country> countries;
	
	//METHODS
	//CONSTRUCTS
	public Region() {
	}

	public Region(int regionId, String regionName) {
		this.regionId = regionId;
		this.regionName = regionName;
	}

	public Region(int regionId, String regionName, Desc_reg descripcion) {
	
		this.regionId = regionId;
		this.regionName = regionName;
		this.descripcion = descripcion;
	}

	public Region(int regionId, String regionName, Desc_reg descripcion, List<Country> countries) {
		
		this.regionId = regionId;
		this.regionName = regionName;
		this.descripcion = descripcion;
		this.countries = countries;
	}
	
	//GETTERS AND SETTERS
	public int getRegionId() {
		return regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	public Desc_reg getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(Desc_reg descripcion) {
		this.descripcion = descripcion;
	}

	public List<Country> getCountries() {
		return countries;
	}

	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}
	
	//TO STRING
	@Override
	public String toString() {
		return "Region [regionId=" + regionId + ", regionName=" + regionName + ", descripcion=" + descripcion
				+ ", countries=" +  countries.size()+"]";
	}
}
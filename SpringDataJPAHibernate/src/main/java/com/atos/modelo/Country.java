package com.atos.modelo;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table (name="COUNTRIES", schema="HR")
public class Country implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	
	
	@Id
	@Column(name="COUNTRY_ID")
	private String countryId;
	
	@Column(name="COUNTRY_NAME")
	private String countryName;
	
	@ManyToOne
	@JoinColumn(name="REGION_ID")
	private Region region;
	
	//METHODS
	//CONSTRUCTS
	public Country() {
		
	}
	
	public Country(String countryId, String countryName) {
		this.countryId = countryId;
		this.countryName = countryName;
	}

	public Country(String countryId, String countryName, Region region) {

		this.countryId = countryId;
		this.countryName = countryName;
		this.region = region;
	}
	
	//GETTERS AND SETTERS
	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}
	 
	//TO STRING
	@Override
	public String toString() {
		return "Country [countryId=" + countryId + ", countryName=" + countryName + ", region=" + region + "]";
	}	
}

package com.atos.modelo;

import java.io.Serializable;

import javax.persistence.*;

@Entity
@Table(name="DESC_REG", schema="HR")
public class Desc_reg  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
		@Id
		@Column(name="ID")
		private int id;
		
		@Column(name="DESCRIPCION")
		private String descripcion;
		
		@OneToOne(fetch= FetchType.LAZY)
		@JoinColumn(name="REGION_ID")
		private Region region;
		
		public Desc_reg() {
			
		}

		public Desc_reg(int id, String descripcion, Region region) {
			super();
			this.id = id;
			this.descripcion = descripcion;
			this.region = region;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getDescripcion() {
			return descripcion;
		}

		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}

		public Region getRegion() {
			return region;
		}

		public void setRegion(Region region) {
			this.region = region;
		}

		@Override
		public String toString() {
			return "Desc_reg [id=" + id + ", descripcion=" + descripcion + ", region=" + region.getRegionId() + "]";
		}

		
		
		
	
}

package com.atos.ejecucion;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.atos.modelo.Region;
import com.atos.servicios.RegionService;

public class Test {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring-config.xml");

		RegionService servicio = (RegionService) ctx.getBean("regionServiceImpl");

		List<Region> regions = servicio.getAll();
		System.out.println("\nNumero regiones inicio: " + regions.size());
		System.out.println(regions.get(0));
		System.out.println("\t"+regions.get(0).getDescripcion());
		/*
		Region newRegion = new Region();
		newRegion.setRegionId(14);
		newRegion.setRegionName("Talavera");

		servicio.add(newRegion);
		System.out.println("\nRegion insertada");

		//Region region2 = servicio.getById(2);
		//region2.setRegionName(region2.getRegionName() + "!!!!");
		//servicio.update(region2);
		
		regions = servicio.getAll();
		System.out.println("\nNumero regiones actuales: " + regions.size());
		
		if( regions != null )System.out.println( "si HAY" );
		else System.out.println(regions);

		//servicio.destroy(1);
		*/
		
		
		regions = servicio.getAll();
		System.out.println("\nNumero regiones finales: " + regions.size());
		
		for (Region r : regions) {
			System.out.println(r);
		}
		ctx.close();
	

	}

}

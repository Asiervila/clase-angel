package com.atos.servicios;

import java.util.List;

import com.atos.modelo.Region;

/*
 * Patron de Fachada (Facade) para abstraccion del DAO
 * mediante una capa intermedia
 */
public interface RegionService {

	void add(Region region);

	void update(Region region);

	void destroy(int regionId);

	Region getById(int regionId);

	List<Region> getAll();

}

package com.atos.controlador;

import java.util.List;

import com.atos.modelo.Desc_reg;


public interface Desc_regDAO {
	
	void create(Desc_reg descripcion);

	void edit(Desc_reg descripcion);

	void delete(int id);

	Desc_reg findById(int id);

	List<Desc_reg> findAll();
}

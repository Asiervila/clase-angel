package com.atos.controlador;

import java.util.List;

import com.atos.modelo.Country;


public interface CountryDAO {
	
	void create(Country country);

	void edit(Country country);

	void delete(int countryId);

	Country findById(int countryId);

	List<Country> findAll();
}

package com.atos.controlador;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.atos.modelo.Region;

/*
 * Configurar componente de Spring mediante anotaciones.
 * 
 * Como es el DAO configurar como repositorio
 */
@Repository
public class RegionDAOImpl implements RegionDAO {

	/*
	 * Configuración de EntityManager para unidad de persistencia
	 */
	@PersistenceContext
	private EntityManager em;
	
	@Override
	public void create(Region region) {
		em.persist(region);
	}

	@Override
	public void edit(Region region) {
		em.merge(region);
	}

	@Override
	public void delete(int regionId) {
		em.remove(em.getReference(Region.class, regionId));
	}

	@Override
	public Region findById(int regionId) {
		return em.find(Region.class, regionId);
	}

	/*
	 * Podemos utilizar consulta JPQL estatica definida en
	 * la entidad @NamedQuery.... y crear con
	 * Query q = em.createNamedQuery
	 * 
	 * Podemos utilizar consulta JPQL dinamica definida y creada con
	 * Query q = em.createQuery
	 * 
	 * Podemos utilizar CriteriaQuery 
	 */
	@Override
	public List<Region> findAll() {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		
		CriteriaQuery<Region> query = 
				builder.createQuery(Region.class);
		
		// Clausula FROM consulta para recuperar datos 
		Root<Region> root = query.from(Region.class);
		
		// Hacer consulta de todos los campos ordenando por
		// campo regionId
		query.select(root).orderBy(builder.asc(root.get("regionId")));
		
		return em.createQuery(query).getResultList();
	}

}

package com.atos.spring.data.jdbc.ejecucion;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.atos.spring.data.jdbc.modelo.Country;
import com.atos.spring.data.jdbc.modelo.CountryDAO;

public class Test2 {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring-configuration.xml");

		CountryDAO dao = (CountryDAO) ctx.getBean("jdbcCountryDAO");
		
		Country newCountry = new Country("RA", "Rusia", 1);
		dao.insert(newCountry);
		
		Country country1 = dao.findById("RA");
		System.out.println(country1);
		
		country1.setCountryName("HolaDonPepito");
		dao.edit(country1);
		
		System.out.println( "\nTODOS" );
		List<Country> countries = dao.findAll();
		for( Country c : countries ) {
			System.out.println(c);
		}
		
		dao.delete("RA");
		
		ctx.close();
	}

}

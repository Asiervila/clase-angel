package com.atos.spring.data.jdbc.modelo;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

public class CountryDAOImpl implements CountryDAO {
	//QUERIES
	private final String INSERT = "INSERT INTO HR.COUNTRIES(COUNTRY_ID, COUNTRY_NAME, REGION_ID) " + "VALUES(?,?,?)";
	private final String EDIT = "UPDATE HR.COUNTRIES SET COUNTRY_NAME = ?, REGION_ID = ? WHERE COUNTRY_ID = ?";
	private final String DELETE = "DELETE FROM HR.COUNTRIES WHERE COUNTRY_ID = ?";
	private final String FIND_BY_ID = "SELECT COUNTRY_ID, COUNTRY_NAME, REGION_ID FROM HR.COUNTRIES WHERE COUNTRY_ID = ?";
	private final String FIND_ALL = "SELECT COUNTRY_ID, COUNTRY_NAME, REGION_ID FROM HR.COUNTRIES ORDER BY COUNTRY_ID";
	
	//Origen de datos configurado con Spring a la BBDD
	private DataSource dataSource;

	//Plantilla para automatizar codigo JDBC
	private JdbcTemplate jdbcTemplate;
	
	//CONSTRUCT
	public CountryDAOImpl(DataSource dataSource) {
		// Crear plantilla asociada a origen de datos
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(this.dataSource);
	}

	@Override
	public void insert(Country country) {
		// Ejecutar instruccion parametrizada
		// Primer argumento => instruccion SQL
		// Segundo argumento => matriz de Object con el valor
		// de los parametros de entrada
		this.jdbcTemplate.update(this.INSERT, new Object[] { country.getCountryId(), country.getCountryName(), country.getRegionId() });
	}

	@Override
	public void edit(Country country) {
		// Ejecutar instruccion parametrizada
		// Primer argumento => instruccion SQL
		// Segundo argumento => matriz de Object con el valor
		// de los parametros de entrada
		this.jdbcTemplate.update(this.EDIT, new Object[] { country.getCountryName(), country.getRegionId(), country.getCountryId() });
	}

	@Override
	public void delete(String idCountry) {
		// Ejecutar instruccion parametrizada
		// Primer argumento => instruccion SQL
		// Segundo argumento => matriz de Object con el valor
		// de los parametros de entrada
		this.jdbcTemplate.update(this.DELETE, new Object[] { idCountry });
	}

	@Override
	public Country findById(String idCountry) {
		// Ejecutar instruccion parametrizada
		// Primer argumento => instruccion SQL
		// Segundo argumento => matriz de Object con el valor
		// de los parametros de entrada
		// Tercer argumento => BeanPropertyRowMapper. Enlaza las
		// columnas del SELECT con los campos del DTO o Entidad (JPA)
		// SIEMPRE QUE TENGAN LOS MISMOS NOMBRES
		Country country = 
				this.jdbcTemplate.queryForObject(this.FIND_BY_ID, 
				new Object[] { idCountry },
				new BeanPropertyRowMapper<>(Country.class));
		
		return country;
	}

	@Override
	public List<Country> findAll() {
		// Ejecutar instruccion parametrizada
		// Primer argumento => instruccion SQL
		// Segundo argumento => BeanPropertyRowMapper. Enlaza las
		// columnas del SELECT con los campos del DTO o Entidad (JPA)
		// SIEMPRE QUE TENGAN LOS MISMOS NOMBRES
		List<Country> countries = 
				this.jdbcTemplate.query(this.FIND_ALL,
				new BeanPropertyRowMapper<>(Country.class));
		
		return countries;
	}

}

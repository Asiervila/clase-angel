package com.atos.spring.data.jdbc.modelo;

import java.io.Serializable;

public class Country implements Serializable{
	//ATTRIBUTES
	private static final long serialVersionUID = 666L;
	
	private String countryId;
	private String countryName;
	private int regionId;
	
	//METHODS
	//CONSTRUCTS
	public Country() {
		
	}
	
	public Country(String countryId, String countryName, int regionId) {
		this.countryId = countryId;
		this.countryName = countryName;
		this.regionId = regionId;
	}

	//GETTERS AND SETTERS
	public String getCountryId() {
		return countryId;
	}

	public void setCountryId(String countryId) {
		this.countryId = countryId;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public int getRegionId() {
		return regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}
	
	//TO STRING
	@Override
	public String toString() {
		return "Country_name [countryId=" + countryId + ", countryName=" + countryName + ", regionId=" + regionId + "]";
	}
}

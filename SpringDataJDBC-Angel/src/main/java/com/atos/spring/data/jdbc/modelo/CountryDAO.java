package com.atos.spring.data.jdbc.modelo;

import java.util.List;

public interface CountryDAO {

	void insert(Country country);

	void edit(Country country);

	void delete(String idCountry);

	Country findById(String idCountry);

	List<Country> findAll();

}

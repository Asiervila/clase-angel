package com.atos.spring.data.jdbc.modelo;

import java.io.Serializable;

public class Region implements Serializable {

	private static final long serialVersionUID = 666L;

	/*
	 * Si se va a utilizar JdbcTemplate y BeanPropertyRowMapper poner campos con el
	 * mismo nombre que las columas con escritura CaMeL REGION_ID, REGIONID =>
	 * regionId
	 */
	private int regionId;
	private String regionName;

	public Region() {
	}

	public Region(int regionId, String regionName) {
		this.regionId = regionId;
		this.regionName = regionName;
	}

	public int getRegionId() {
		return regionId;
	}

	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	@Override
	public String toString() {
		return "\nRegion [regionId=" + regionId + ", regionName=" + regionName + "]";
	}

}

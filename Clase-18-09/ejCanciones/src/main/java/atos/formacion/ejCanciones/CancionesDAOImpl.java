package atos.formacion.ejCanciones;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;







@Service
public class CancionesDAOImpl implements CancionesDAO {
	
private static List<Cancion> canciones;

	
	static {
		
		canciones = new ArrayList<>();

		canciones.add(new Cancion("titulo1", " Melendi", "album1", 1995));
		canciones.add(new Cancion("titulo2", "Melendi", "album1", 2003));
		canciones.add(new Cancion("titulo3", "Fito&Fitipaldis", "album2", 2010));
		canciones.add(new Cancion("titulo4:", "Julio Iglesias ", "album5", 1970));
		

	}
	

	
	public List<Cancion> obtenerCanciones() {
		
		return canciones;
	}

	
	public List<Cancion> obtenerAlbum(String album) {
										//o null;
		List<Cancion> canciones_tmp = new ArrayList();
		for (Cancion cancion : canciones) {
			if (cancion.getAlbum().equals(album)) {
				canciones_tmp.add(cancion);
			}
		}
		return canciones_tmp;
	}
	
	
	public Cancion obtenerTitulo(String titulo) {
		for( Cancion cancion : canciones) {
			if(cancion.getTitulo().equals(titulo)) {
				return cancion;
			}
		}
		return null;
	}
	
	public Cancion borrarCancion(String bCancion) {

		canciones.remove(obtenerTitulo(bCancion));

		return null;
	}
	public Cancion AñadirCancion(Cancion aCancion) {
		canciones.add(new Cancion("titulo5","Melendi","album1",2004));
		return null;
	}
	
}

package atos.formacion.ejCanciones;

import java.io.Serializable;
import java.util.Date;

/*
 * Entidad que encapsula una entrada de un blog
 * 
 * Clase inmutable => Una vez creado un objeto No se puede cambiar
 * 
 * Campos finales e inicializamos en constructor
 */
public class Cancion {

	private static final long serialVersionUID = 666L;

	private static int secuencia = 101;

	private final String titulo;
	private final String artista;
	private final String album;
	private final int año;
	
	public Cancion(String titulo, String artista, String album, int año) {
		super();
		this.titulo = titulo;
		this.artista = artista;
		this.album = album;
		this.año = año;
	}

	
	
	public static int getSecuencia() {
		return secuencia;
	}

	public static void setSecuencia(int secuencia) {
		Cancion.secuencia = secuencia;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getTitulo() {
		return titulo;
	}

	public String getArtista() {
		return artista;
	}

	public String getAlbum() {
		return album;
	}

	public int getAño() {
		return año;
	}
	
}


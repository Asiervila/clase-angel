package atos.formacion.ejCanciones;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CancionesController {

	@Autowired
	CancionesDAOImpl servicio;
	
	@RequestMapping(value = "/canciones", method = RequestMethod.GET)
	public List<Cancion> todasLasCanciones() {
	
		return  servicio.obtenerCanciones();
	}
	//{valor que se pasa ej album1}
	@RequestMapping("/canciones/{album}")
	public List<Cancion> getAlbum(@PathVariable("album") String album) {
		return  servicio.obtenerAlbum(album);
		
	}
	
	@RequestMapping("/cancion/{titulo}")
	public Cancion getTitulo(@PathVariable("titulo") String titulo) {
		
		return  servicio.obtenerTitulo(titulo);
		
	}
	@RequestMapping(value="/cancion/{titulo}", method = RequestMethod.DELETE)
	public Cancion borrar(@PathVariable("titulo") String titulo) {
		
	
		return servicio.borrarCancion(titulo);
		
	}
	@RequestMapping(value = "/canciones", method = RequestMethod.POST)
	public Cancion añadir(@RequestBody Cancion cancion) {
		
		return servicio.AñadirCancion(cancion);
		
	}
	//PUT modificar
	@RequestMapping(value = "/canciones", method = RequestMethod.PUT)
	public Cancion modificar(@RequestBody Cancion cancion) {
		return null;
	}
	
	

}

package atos.formacion.ejCanciones;

import java.util.List;


public interface CancionesDAO {
	
	List<Cancion> obtenerCanciones();
	

	List<Cancion> obtenerAlbum(String album);
	
	
	Cancion obtenerTitulo(String titulo);
	
	Cancion borrarCancion(String cancion);
	}


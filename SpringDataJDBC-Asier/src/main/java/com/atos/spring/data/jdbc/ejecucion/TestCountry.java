package com.atos.spring.data.jdbc.ejecucion;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.atos.spring.data.jdbc.modelo.Country;
import com.atos.spring.data.jdbc.modelo.CountryDAO;

public class TestCountry {
	
	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext ctx = new
				ClassPathXmlApplicationContext("spring-configuration.xml");
		
		
		CountryDAO  dao =
				(CountryDAO) ctx.getBean("jdbcCountryDAO");
		
		
		Country newCountry = new Country("PP","MEJOR",3);
		
		//dao.insert(newCountry);
		
		Country countryB = dao.findById("ES");
		System.out.println(countryB);
		countryB.setRegionId(1);
		System.out.println(countryB);
		System.out.println(dao.findAll());
	
		newCountry.setCountryName("ESPA�A");
		dao.edit(newCountry);
		
		//dao.delete(newCountry.getCountryId());
		ctx.close();
	}
}

package com.atos.spring.data.jdbc.modelo;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

public class CountryDAOImpl  implements CountryDAO{

	//Origen de datos:
	private DataSource dataSource;
	
	//Plantilla
	
	private JdbcTemplate jdbcTemplate;
	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	public void insert(Country country) {
		
		String sql= "INSERT INTO HR.COUNTRIES1(COUNTRY_ID, COUNTRY_NAME, REGION_ID) VALUES(?,?,?)";
		 
		//Creamos la plantilla
		
		jdbcTemplate = new JdbcTemplate(dataSource);
		//sql instruccion parametrizada
		//Object objeto con el valor del os parametros de entrada
		jdbcTemplate.update(sql, new Object[] {country.getCountryId(), country.getCountryName(), country.getRegionId()});
		
	}

	@Override
	public void edit(Country country) {
		
		String sql="UPDATE HR.COUNTRIES1 SET COUNTRY_NAME= ?, REGION_ID= ? WHERE COUNTRY_ID=?";
		
		//Creamos plantilla o.Datos
		jdbcTemplate = new JdbcTemplate(dataSource);
		
		jdbcTemplate.update(sql, new Object[] { country.getCountryName(), country.getRegionId(), country.getCountryId()});
		
	}

	@Override
	public void delete(String countryId) {
		
		String sql = "SELECT COUNTRY_ID, COUNTRY_NAME, REGION_ID FROM HR.COUNTRIES1 WHERE COUNTRY_ID = ?";
		
		jdbcTemplate.update(sql, new Object[] {countryId});
	}

	@Override
	public Country findById(String countryId) {
		
		String sql = "SELECT COUNTRY_ID, COUNTRY_NAME FROM HR.COUNTRIES1 WHERE COUNTRY_ID = ?";
		
		jdbcTemplate = new JdbcTemplate(dataSource);
		
		Country country =
				jdbcTemplate.queryForObject(sql, new Object[] {countryId}, new BeanPropertyRowMapper<>(Country.class));
		
		
		return country;
	
		
	}

	@Override
	public List<Country> findAll() {
		
		
		String sql = "SELECT COUNTRY_ID, COUNTRY_NAME FROM HR.COUNTRIES1 ORDER BY COUNTRY_ID";
		
		jdbcTemplate = new JdbcTemplate(dataSource);
		
		List<Country> paises = 
				jdbcTemplate.query(sql, new BeanPropertyRowMapper<>(Country.class));

		return paises;

	}

	
}

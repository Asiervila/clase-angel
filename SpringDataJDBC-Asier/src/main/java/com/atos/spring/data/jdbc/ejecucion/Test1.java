package com.atos.spring.data.jdbc.ejecucion;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.atos.spring.data.jdbc.modelo.Region;
import com.atos.spring.data.jdbc.modelo.RegionDAO;

public class Test1 {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext ctx = new
				ClassPathXmlApplicationContext(
						"spring-configuration.xml");

		RegionDAO dao = 
				(RegionDAO) ctx.getBean("jdbcRegionDAO");
		
		Region newRegion = new Region(5, "Talavera");
		dao.insert(newRegion);
		
		Region region1 = dao.findById(1);
		System.out.println(region1);
		
		region1.setRegionName("Europe");
		dao.edit(region1);
		
		System.out.println(dao.findAll());
		
		dao.delete(5);
		
		ctx.close();
	}

}

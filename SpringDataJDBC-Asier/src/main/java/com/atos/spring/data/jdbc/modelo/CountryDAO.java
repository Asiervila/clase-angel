package com.atos.spring.data.jdbc.modelo;

import java.util.List;

public interface CountryDAO {
		
	void insert(Country country);
	
	void edit (Country country);
	
	void delete (String countryId);
	
	List<Country> findAll();

	Country findById(String countryId);
}
